import { Component, ViewEncapsulation } from '@angular/core';
import { AuthenticationService } from './loginPage/_services';
import { Router } from '@angular/router';
import { User } from './loginPage/_models/user';
import './loginPage/_content/app.less';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  currentUser: User;
  title = 'SocialQueens';
  route: string;

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router ) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    router.events.subscribe(() => this.route = router.url);
}

logout() {
  this.authenticationService.logout();
  this.router.navigate(['/login']);
}
}
