﻿import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { User } from '../_models';
import { UserService, AuthenticationService } from '../_services';

@Component({ templateUrl: 'home.component.html',
styleUrls: ['./home.component.css'] })
export class HomeComponent implements OnInit {
    currentUser: User;
    users = [];
    showMainContent = true;

    constructor(
        private authenticationService: AuthenticationService,
        private userService: UserService,
        private router: Router
    ) {
        this.currentUser = this.authenticationService.currentUserValue;
    }

    ngOnInit() {
        this.loadAllUsers();
    }

    deleteUser(id: number) {
        this.userService.delete(id)
            .pipe(first())
            .subscribe(() => this.loadAllUsers());
    }
    // Po usunięciu jeszcze chwilę wyświetla się informacja u userze...
    deleteUserAndLogOff(id: number) {
      const isConfirmed = confirm('Are you sure you want to delete your account?');
      if (!isConfirmed) {
        return;
      }
      this.showMainContent = false;
      this.deleteUser(id);
      this.authenticationService.logout();
  }

    private loadAllUsers() {
        this.userService.getAll()
            .pipe(first())
            .subscribe(users => this.users = users);
    }

    logout() {
      this.authenticationService.logout();
      this.router.navigate(['/login']);
    }

}
