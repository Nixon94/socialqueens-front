﻿import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';
import { Guid } from 'guid-typescript';
import { isNullOrUndefined } from 'util';

// arrays in local storage
let users = JSON.parse(localStorage.getItem('users')) || [];
let models = JSON.parse(localStorage.getItem('models')) || [];

const modelsList: any[] = [
   {name: 'Jessica', surname: 'Alba', appName: 'Jessie',
  instagramLink: 'https://www.instagram.com/jessicaalba/',
  yearOfBirth: 1981, height: 169, weight: 54, country: 'USA',
  city: 'Los Angeles', avatar: 'https://i.iplsc.com/-/0005RL6F5LMBOKR5-C430.jpg',
  rating: 4.92, services: ['Travel', 'Dinner', 'Party'], languages: ['English', 'Spanish']},
  { name: 'Adriana', surname: 'Lima', appName: 'Adriana',
  instagramLink: 'https://www.instagram.com/adrianalima/',
  yearOfBirth: 1981, height: 178, weight: 58, country: 'Brazil',
  city: 'Salvador',
  avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQxOoeUFMBvUj_CYeLZaoJqTZ2zkYDjQ79GDRclWVkziefQbr7H',
  rating: 4.81, services: ['Dinner', 'Party', 'Photoshoot'], languages: ['English', 'Spanish']},
  { name: 'Allesandra', surname: 'Ambrosio', appName: 'Alli',
  instagramLink: 'https://www.instagram.com/alessandraambrosio/',
  yearOfBirth: 1981, height: 177, weight: 57, country: 'USA',
  city: 'New York',
  avatar: 'https://articlebio.com/uploads/bio/2015/08/13/alessandra-ambrosio.jpg',
  rating: 4.97, services: ['Dinner', 'Party', 'Holidays'], languages: ['English', 'Italian', 'Spanish']},
  { name: 'Barbara ', surname: 'Palvin', appName: 'Barbie',
  instagramLink: 'https://www.instagram.com/realbarbarapalvin/',
  yearOfBirth: 1993, height: 175, weight: 55, country: 'Hungary',
  city: 'Budapest',
  avatar: 'https://stellameus.com/wp-content/uploads/2019/02/Barbara-Palvin-la-altura-peso-parametros-385x405.jpg',
  rating: 4.760, services: ['Party', 'Holidays'], languages: ['English', 'Hungarian', 'Spanish']},
  {name: 'Irina  ', surname: 'Shayk', appName: 'Iri',
  instagramLink: 'https://www.instagram.com/irinashayk/',
  yearOfBirth: 1986, height: 178, weight: 55, country: 'USA',
  city: 'Los Angeles',
  avatar: 'https://pbs.twimg.com/profile_images/1139286822821990402/xVmHFBwA_400x400.png',
  rating: 4.88, services: ['Dinner', 'Holidays', 'Photoshoot'], languages: ['English', 'Russian']}
];

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const { url, method, headers, body } = request;

        // wrap in delayed observable to simulate server api call
        return of(null)
            .pipe(mergeMap(handleRoute))
            // tslint:disable-next-line: max-line-length
            .pipe(materialize()) // call materialize and dematerialize to ensure delay even if an error is thrown (https://github.com/Reactive-Extensions/RxJS/issues/648)
            .pipe(delay(500))
            .pipe(dematerialize());

        function handleRoute() {
            switch (true) {
                case url.endsWith('/users/authenticate') && method === 'POST':
                    return authenticate();
                case url.endsWith('/users/register') && method === 'POST':
                    return register();
                case url.endsWith('/users') && method === 'GET':
                    return getUsers();
                case url.match(/\/users\/\d+$/) && method === 'DELETE':
                    return deleteUser();
                case url.endsWith('/models/populate') && method === 'GET':
                      return populateModels();
                case url.endsWith('/models') && method === 'GET':
                    return getModels();
                case url.match(/\/models\/[\s\S]*/) && method === 'DELETE':
                    return deleteModel();
                default:
                    // pass through any requests not handled above
                    return next.handle(request);
            }
        }

        // route functions

        function authenticate() {
            const { username, password } = body;
            const user = users.find(
            (x => x.username === username && x.password === password) ||
            (x => x.email === username && x.password === password));
            if (user) {
              return ok({
                  id: user.id,
                  username: user.username,
                  firstName: user.firstName,
                  lastName: user.lastName,
                  email: user.email,
                  token: 'fake-jwt-token'
              });
            } else { return error('Username or password is incorrect'); }
        }

        // User functions

        function register() {
            const user = body;

            if (users.find(x => x.username === user.username)) {
                return error('Username "' + user.username + '" is already taken');
            }
            if (users.find(x => x.email === user.email)) {
              return error('Email "' + user.email + '" was already used');
          }

            user.id = users.length ? Math.max(...users.map(x => x.id)) + 1 : 1;
            users.push(user);
            localStorage.setItem('users', JSON.stringify(users));

            return ok();
        }

        function getUsers() {
            if (!isLoggedIn()) { return unauthorized(); }
            return ok(users);
        }

        function deleteUser() {
            if (!isLoggedIn()) { return unauthorized(); }

            users = users.filter(x => x.id !== stringIdFromUrl());
            localStorage.setItem('users', JSON.stringify(users));
            return ok();
        }

        // Model functions

        function populateModels() {
          if (!isLoggedIn()) { return unauthorized(); }

          for (const model of modelsList) {
            const exist = models.map(m => m.instagramLink).indexOf(model.instagramLink) !== -1;
            if (!exist) {
              const tempModel = Object.assign({}, model);
              tempModel.id = Guid.create().toString();
              models.push(tempModel);
            }
          }
          localStorage.setItem('models', JSON.stringify(models));
          return ok();
        }

        function getModels() {
          if (!isLoggedIn()) { return unauthorized(); }
          return ok(models);
        }

       // to be developed further...
        function deleteModel() {
         if (!isLoggedIn()) { return unauthorized(); }

         models = models.filter(x => x.id !== stringIdFromUrl());
         localStorage.setItem('models', JSON.stringify(models));
         return ok();
     }

        // helper functions

        // tslint:disable-next-line: no-shadowed-variable
        function ok(body?) {
            return of(new HttpResponse({ status: 200, body }));
        }

        function error(message) {
            return throwError({ error: { message } });
        }

        function unauthorized() {
            return throwError({ status: 401, error: { message: 'Unauthorised' } });
        }

        function isLoggedIn() {
            return headers.get('Authorization') === 'Bearer fake-jwt-token';
        }

        function idFromUrl() {
            const urlParts = url.split('/');
            // tslint:disable-next-line: radix
            return parseInt(urlParts[urlParts.length - 1]);
        }

        function stringIdFromUrl() {
          const urlParts = url.split('/');
          // tslint:disable-next-line: radix
          return (urlParts[urlParts.length - 1]).toString();
      }


    }
}

export const fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: HTTP_INTERCEPTORS,
    useClass: FakeBackendInterceptor,
    multi: true
};
