import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Model} from '../../models/shared/frontModel.model';

@Injectable({ providedIn: 'root' })
export class FrontModelService {
    constructor(private http: HttpClient) { }

    getAll() {
      return this.http.get<Model[]>(`/models`);
    }

    add(model: Model) {
        return this.http.post(`/models`, model);
    }

    delete(id: string) {
        return this.http.delete(`/models/${id}`);
    }

    populate() {
      return this.http.get(`/models/populate`);
  }
}
