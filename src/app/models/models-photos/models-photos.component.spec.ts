import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelsPhotosComponent } from './models-photos.component';

describe('ModelsPhotosComponent', () => {
  let component: ModelsPhotosComponent;
  let fixture: ComponentFixture<ModelsPhotosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelsPhotosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelsPhotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
