import { Component, OnInit, HostBinding, Input, Output, EventEmitter } from '@angular/core';
import {Model} from '../shared/model.model';

@Component({
  selector: 'app-models-photos',
  templateUrl: './models-photos.component.html',
  styleUrls: ['./models-photos.component.css']
})
export class ModelsPhotosComponent implements OnInit {
  @HostBinding('attr.class') cssClass = 'background-color: red';
  @Input() imageUrl: string;
  @Input() model: Model;
  @Output() modelSelected: EventEmitter<Model>;

  constructor() {
    this.modelSelected = new EventEmitter<Model>();
  }

  ngOnInit() {
  }

  createPath(): string {
    return this.imageUrl || '/assets/images/default_model_avatar.jpg';
  }

  clicked() {
    this.modelSelected.emit(this.model);
  }
}
