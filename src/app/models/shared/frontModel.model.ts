import { Guid } from 'guid-typescript';

export class Model {
  id: string;
  name: string;
  surname: string;
  appName: string;
  instagramLink: string;
  yearOfBirth: number;
  height: number;
  weight: number;
  country: string;
  city: string;
  avatar: string;
  rating: number;
  services: string[];
  languages: string[];
}
