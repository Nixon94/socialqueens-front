import { Injectable } from '@angular/core';
import { ApiService } from '../../shared/api.service';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '../../loginPage/_services';
import { environment } from '../../../environments/environment';
import { Model } from './model.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ModelsService extends ApiService {

  constructor(
    httpClient: HttpClient,
    authService: AuthenticationService) {
    super(
      environment.apiUrl,
      httpClient,
      authService);
  }

  search(match?: string): Observable<Model[]> {
    return super.get<Model[]>(match, true);
  }

  download(id: string): Observable<Model> {
    return super.get<Model>(id, true);
  }

  add(model: Model): Observable<any> {
    return super.post('', model, true);
  }

  remove(model: Model): Observable<any> {
    return super.delete<Model>(`${model.id}`, true);
  }

  update(model: Model): Observable<any> {
    return super.put(`${model.id}`, model, true);
  }

  addRating(model: Model, rating: object): Observable<any> {
    return super.put(`${model.id}/rating`, rating, true);
  }
}
