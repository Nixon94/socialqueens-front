import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModelsPhotosComponent } from './models-photos/models-photos.component';
import { ModelsListComponent } from './models-list/models-list.component';
import { ModelsRoutingModule } from './models-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ModelsCardComponent } from './models-card/models-card.component';


@NgModule({
  declarations: [ModelsPhotosComponent, ModelsListComponent, ModelsCardComponent],
  imports: [
    CommonModule,
    ModelsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ]
})
export class ModelsModule { }
