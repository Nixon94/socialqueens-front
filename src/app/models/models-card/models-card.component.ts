import { Component, OnInit, AfterViewInit, ElementRef, ViewEncapsulation } from '@angular/core';
import { ModelsService } from '../shared/models.service';
import { Model } from '../shared/model.model';
import { fromEvent, of } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap, catchError } from 'rxjs/operators';

@Component({
  selector: 'app-models-card',
  templateUrl: './models-card.component.html',
  styleUrls: ['./models-card.component.css'],
  encapsulation: ViewEncapsulation.Emulated
})
export class ModelsCardComponent implements OnInit {

  constructor(private readonly service: ModelsService) { }

  models: Model[];
  newModelName: string;
  model: Model = new Model();
  public selectedName: string;
  params = '';
  searchBox: HTMLInputElement;
  title: string;

  ngOnInit() {
    this.searchModels();
  }


  searchModels(): void {
    this.service.search(`?Service=${this.params}`)
      .subscribe(models => this.models = models);
  }

  getModels$() {
    return this.service.search(`?Service=${this.params}`);
  }

  ngAfterViewInit(): void {
    this.searchBox = document.getElementById('search-box') as HTMLInputElement;
    fromEvent(this.searchBox, 'input').pipe(
      map((e: KeyboardEvent) => { const target = e.target as HTMLInputElement; return target.value; }),
      filter(text => text.length > 2),
      debounceTime(1000),
      distinctUntilChanged(),
      switchMap(() => this.getModels$()
        .pipe(catchError(() => of(this.models)))
      )
    ).subscribe(ee => this.models = ee,
      e => console.log(e));
  }

  public highlightRow(model) {
    this.selectedName = model.id;
  }

  public modelWasSelected(model: Model) {
    console.log(model);
  }

  public modelAge(model:Model){
    return new Date().getFullYear() - model.yearOfBirth;
  }
}
