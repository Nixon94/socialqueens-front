import { NgModule } from '@angular/core';
import { ModelsListComponent } from './models-list/models-list.component';
import { Routes, RouterModule } from '@angular/router';
import { ModelsCardComponent } from './models-card/models-card.component';





const routes: Routes = [
  {path: 'models', component: ModelsListComponent},
  {path: 'clients', component: ModelsCardComponent},
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModelsRoutingModule { }
