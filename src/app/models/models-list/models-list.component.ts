import { Component, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { ModelsService } from '../shared/models.service';
// import { Model } from '../shared/model.model';
import {Model} from '../shared/frontModel.model';
import { fromEvent, of } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap, catchError } from 'rxjs/operators';
import { FrontModelService } from './../../loginPage/_services/frontModel.service';

@Component({
  selector: 'app-models-list',
  templateUrl: './models-list.component.html',
  styleUrls: ['./models-list.component.css']
})
export class ModelsListComponent implements OnInit {

  constructor(private readonly service: ModelsService, private frontModelService: FrontModelService, private elementRef: ElementRef) { }

  models: Model[];
  public selectedName: string;
  params = '';
  searchBox: HTMLInputElement;

  ngOnInit() {
    this.getFakeModels$()
    .subscribe(models => this.models = models);
  }

  getFakeModels$() {
    return this.frontModelService.getAll();
  }

  // searchModels(): void {
  //   this.service.search(`?Service=${this.params}`)
  //     .subscribe(models => this.models = models);
  // }

  getModels$() {
    return this.service.search(`?Service=${this.params}`);
  }

  ngAfterViewInit(): void {
    this.searchBox = document.getElementById('search-box') as HTMLInputElement;
    fromEvent(this.searchBox, 'input').pipe(
      map((e: KeyboardEvent) => { const target = e.target as HTMLInputElement; return target.value; }),
      filter(text => text.length > 2),
      debounceTime(1000),
      distinctUntilChanged(),
      switchMap(() => this.getFakeModels$()
        .pipe(catchError(() => of(this.models)))
      )
    ).subscribe(ee => this.models = ee,
      e => console.log(e));
  }

  public highlightRow(model) {
    this.selectedName = model.id;
  }

  public modelWasSelected(model: Model) {
    console.log(model);
  }

  public calculateAge(model: Model) {
    return new Date().getFullYear() - model.yearOfBirth;
  }

  async populateModels() {
    return this.frontModelService.populate().subscribe();
  }

  async deleteModel(model: Model) {
    const isConfirmed = confirm('Are you sure?');
    if (!isConfirmed) {
      return;
    }
    return this.frontModelService.delete(model.id).subscribe();
  }

  async deleteModelAndRefreshList(model: Model) {
    this.deleteModel(model);
    this.getFakeModels$().subscribe(models => this.models = models);
  }

  // TODO add new component with all the necessary fields
  async addModel() {

  }



}
