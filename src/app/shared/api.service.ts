import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../loginPage/_services';


export abstract class ApiService {

  protected constructor(
    private readonly host: string,
    private readonly httpClient: HttpClient,
    private readonly authService: AuthenticationService) { }

  protected get<T>(url: string, isAuthorized: boolean = false): Observable<T> {
    console.log('in search ' + url);
    return this.request('GET', url, null, isAuthorized);
  }

  protected post<T>(url: string, body: any, isAuthorized: boolean = false): Observable<T> {
    return this.request('POST', url, body, isAuthorized);
  }

  protected put<T>(url: string, body: any, isAuthorized: boolean = false): Observable<T> {
    return this.request('PUT', url, body, isAuthorized);
  }

  protected delete<T>(url: string, isAuthorized: boolean = false): Observable<T> {
    return this.request('DELETE', url, null, isAuthorized);
  }

  private request<T>(method: string, url: string, body: any | null, isAuthorized: boolean): Observable<T> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json'}),
      body,
      params: {}
    };

    // if (isAuthorized) {
    // }
    const token = 'secret';
    httpOptions.headers = httpOptions.headers.append('Authorization', token);

    console.log(`${this.host}/${url}`);
    return this.httpClient.request<T>(method, `${this.host}/${url}`, httpOptions);
  }
}
